#define _CRT_SECURE_NO_WARNINGS
#include "comb.h"

int comparator(const void* request1, const void* request2) {
    Request* request_a = (Request*)request1;
    Request* request_b = (Request*)request2;

    return request_a->time_taken - request_b->time_taken;
}

Request* create_request_list(FILE* input) {

    fscanf(input, "%d", &req_amount);
    Request* requests = (Request*)calloc(req_amount, sizeof(Request));

    Time time_taken, time_released;

    for (int counter = 0; counter < req_amount; counter++) {
        fscanf(input, "%d %hd:%hd %hd:%hd", &(requests[counter].mem_vol), &(time_taken.hours), &(time_taken.minutes), &(time_released.hours), &(time_released.minutes));
        requests[counter].time_taken = time_taken.hours * 60 + time_taken.minutes;
        requests[counter].time_released = time_released.hours * 60 + time_released.minutes;
    }

    return requests;
}

int minMemory(Request* requests)
{
	int mem_taken = 0, min_mem_required = 0, last_minute = 0;
	
    qsort(requests, req_amount, sizeof(Request), comparator);
	
    for (int i = 0; i < req_amount; i++) {
        if (last_minute < requests[i].time_released) {
            last_minute = requests[i].time_released;
        }
    }

    for (int current_minute = requests[0].time_taken; current_minute < last_minute; current_minute++) {
        for (int request_counter = 0; request_counter < req_amount; request_counter++) {
            if (requests[request_counter].time_taken == current_minute) {
                requests[request_counter].current_byte = 1;
            	for (int i = 0; i < request_counter; i++)
            	{
            		if(requests[i].current_byte != -1)
            		{
            			if ((requests[i].current_byte <= requests[request_counter].current_byte + requests[request_counter].mem_vol - 1) 
                            && (requests[request_counter].current_byte <= requests[i].current_byte + requests[i].mem_vol - 1))
            			{
                            requests[request_counter].current_byte = requests[i].current_byte + requests[i].mem_vol;
                            i = 0;
            			}
            		}
            	}
                
                if (requests[request_counter].current_byte + requests[request_counter].mem_vol - 1 > min_mem_required)
                {
                    min_mem_required = requests[request_counter].current_byte + requests[request_counter].mem_vol - 1;
                }
            }
            if (requests[request_counter].time_released == current_minute) {
                requests[request_counter].current_byte = -1;
            }
        }
    }
    return min_mem_required;
}