#define _CRT_SECURE_NO_WARNINGS
#include "comb.h"
#include <iostream>
#include <ctime>

using namespace std;

int req_amount = 0;

int main() {
    setlocale(LC_ALL, "RUS");
    FILE* input = fopen("input1000_2.txt", "r");

    Request* requests = create_request_list(input);
    cout << minMemory(requests);
    return 0;
}
