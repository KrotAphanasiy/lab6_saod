#ifndef COMB_H
#define COMB_H
#include <stdio.h>
#include <stdlib.h>


extern int req_amount;

struct Time {
    unsigned short int hours;
    unsigned short int minutes;
};

struct Request {
    int mem_vol = 0;
    int time_taken = 0;
    int time_released = 0;
    int current_byte = -1;
};

Request* create_request_list(FILE* input);
int minMemory(Request* requests);
#endif
